var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 250, window.innerWidth/window.innerHeight, 0.1, 100000 );

var elf;

var renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

var cubeGeometry = new THREE.BoxGeometry( 1, 4, 1 );
var cubeMaterial = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
var cube = new THREE.Mesh( cubeGeometry, cubeMaterial );
var cube_bsp = new ThreeBSP( cube );

var sphereGeometry = new THREE.SphereGeometry( 1, 32, 32 );
var sphereMaterial = new THREE.MeshBasicMaterial( {color: 0xffff00} );
var sphere = new THREE.Mesh( sphereGeometry, sphereMaterial );
var sphere_bsp = new ThreeBSP( sphere );
sphere.name = "ahskh";

var subtract_bsp = sphere_bsp.subtract( cube_bsp );
var result = subtract_bsp.toMesh( new THREE.MeshBasicMaterial( { color: 0x0fff00 } ));

scene.add( result );
//scene.add( cube );
//scene.add( sphere );

var light = new THREE.PointLight(scene, {
    color: 0x000000,
    intensity: 10,
    distance: 10000 
});
light.position.set( 0, 50, 0 );
scene.add( light );

/*var manager = new THREE.LoadingManager();
var onProgress = function ( xhr ) {
    if ( xhr.lengthComputable ) {
        var percentComplete = xhr.loaded / xhr.total * 100;
        console.log( Math.round(percentComplete, 2) + '% downloaded' );
    }
};
var onError = function ( xhr ) {
};*/
//var loader = new THREE.OBJLoader( );
var house;
/*loader.load( '3DModels/beach_home.dae', function ( object ) {
    object.traverse( function ( child ) {
        if ( child instanceof THREE.Mesh ) {
            //child.material.map =  new THREE.MeshBasicMaterial( {color: 0x000000} );;
        }
        child.position.y = 0;
        child.scale = [5, 5, 5]
    } );
    house = object;
    scene.add( house );
}, onProgress, onError );*/

/*var loadingManager = new THREE.LoadingManager( function() {
    scene.add( elf );
} );*/

/*var loader = new THREE.ColladaLoader( loadingManager );
loader.load( 'https://s3.amazonaws.com/babylonjsassets/beach_home.dae', function ( collada ) {
    console.log("'collada'");
    console.log(collada);
    //elf = collada.scene;
} );*/


var loader = new THREE.ColladaLoader();
//loader.options.convertUpAxis = true;
loader.load('https://s3.amazonaws.com/babylonjsassets/beach_home.dae', function ( collada ) {
    scene.add( collada );
} );

/*var geometryX = new THREE.Geometry(scene);
geometryX.vertices.push(new THREE.Vector3(-10000, 0, 0), new THREE.Vector3(10000, 0, 0));
var geometryY = new THREE.Geometry(scene);
geometryY.vertices.push(new THREE.Vector3(0, -10000, 0), new THREE.Vector3(0, 10000, 0));
var geometryZ = new THREE.Geometry(scene);
geometryZ.vertices.push(new THREE.Vector3(0, 0, -10000), new THREE.Vector3(0, 0, 10000));


// lines
var axisX = new THREE.Line( geometryX, new THREE.LineBasicMaterial( { color: 0xff0000, opacity: 1 } ) );
var axisY = new THREE.Line( geometryY, new THREE.LineBasicMaterial( { color: 0x00ff00, opacity: 1 } ) );
var axisZ = new THREE.Line( geometryZ, new THREE.LineBasicMaterial( { color: 0x0000ff, opacity: 1 } ) );
scene.add( axisX );
scene.add( axisY );
scene.add( axisZ );

var materialArray = [];
materialArray.push(new THREE.MeshBasicMaterial( {color: 0xffffff} ));
materialArray.push(new THREE.MeshBasicMaterial( {color: 0xffffff} ));
materialArray.push(new THREE.MeshBasicMaterial( {color: 0xffffff} ));
materialArray.push(new THREE.MeshBasicMaterial( {color: 0xffffff} ));
materialArray.push(new THREE.MeshBasicMaterial( {color: 0xffffff} ));
materialArray.push(new THREE.MeshBasicMaterial( {color: 0xffffff} ));
for (var i = 0; i < 6; i++)
materialArray[i].side = THREE.BackSide;
var skyboxMaterial = new THREE.MeshFaceMaterial( materialArray );
var skyboxGeom = new THREE.CubeGeometry( 50000, 50000, 50000, 1, 1, 1 );
var skybox = new THREE.Mesh( skyboxGeom, skyboxMaterial );
scene.add( skybox );

var controls = new THREE.OrbitControls( camera );
camera.position.set( 0, 0, 10 );
controls.update();*/


var animate = function () {
    requestAnimationFrame( animate );

    //house.rotation.x = 90
    //house.material = new THREE.MeshBasicMaterial( {color: 0xff00ff} );
    //cube.rotation.x += 0.1;
    ///cube.rotation.y += 0.1;
    //controls.update();
    //console.log(result)
    //console.log(sphere)
    renderer.render(scene, camera);
};

animate();